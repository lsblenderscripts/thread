#!BPY

"""
Name: 'Thread'
Blender: 244
Group: 'AddMesh'
"""

__author__= ['Luis Sergio S. Moura Jr.']
__url__ = ("liquidblue.com.br")
__version__= '0.3'
__bpydoc__= '''\

Thread Mesh

0.3 - 2007-07-29 by Luis Sergio<br />
- shoulder

0.2 - 2007-07-29 by Luis Sergio<br />
- renamed from bolt to thread
- bugfix: only 8-sided thread working
- edge crease option

0.1 - 2007-07-27 by Luis Sergio<br />
- initial version

'''

# *** BEGIN LICENSE BLOCK ***
# Creative Commons 3.0 by-sa
#
# Full license: http://creativecommons.org/licenses/by-sa/3.0/
# Legal code: http://creativecommons.org/licenses/by-sa/3.0/legalcode
#
# You are free:
# * to Share  to copy, distribute and transmit the work
# * to Remix  to adapt the work
#
# Under the following conditions:
# * Attribution. You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
# * Share Alike. If you alter, transform, or build upon this work, you may distribute the resulting work only under the same, similar or a compatible license.
#
# * For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.
# * Any of the above conditions can be waived if you get permission from the copyright holder.
# * Nothing in this license impairs or restricts the author's moral rights.
# *** END LICENSE BLOCK ***


import BPyAddMesh
import Blender
from Blender.Window import EditMode
import bpy
import BPyMessages
import math

# Returns a 3d vector given angle, radius and z (optional)
def point(ANGLE, RADIUS, DZ = 0):
	vert = [RADIUS * math.sin(ANGLE), RADIUS * math.cos(ANGLE), DZ]
	return vert


# actually creates the vertices and the faces for the bolt.
def create_bolt(RADIUS, SUBDIVISIONS, STEP, LOOPS, IDENT, SHOULDER):
	verts = []
	faces = []

	topcap = []
	shouldercap = []

	# ========= VERTS =========
	# cap
	for i in range(SUBDIVISIONS):
		# we start one loop BEFORE the other loops, so the 0 is one step before the others.
		deg = math.pi * 2 * (i - 1) / SUBDIVISIONS
		if (i == 0):
			verts.append(point(deg, RADIUS - IDENT))
		else:
			verts.append(point(deg, RADIUS))

	# 1st loop
	for i in range(SUBDIVISIONS):	
		deg = math.pi * 2 * i / SUBDIVISIONS
		space = (i + 1) * STEP / (SUBDIVISIONS * 2)
		verts.append(point(deg, RADIUS - IDENT, space))
		verts.append(point(deg, RADIUS, space * 2))

	# other loops
	disp = 0
	space = STEP / 2
	for j in range(1, LOOPS - 1):
		for i in range(SUBDIVISIONS):
			deg = math.pi * 2 * i / SUBDIVISIONS
			disp = disp + STEP / SUBDIVISIONS
			verts.append(point(deg, RADIUS - IDENT, disp + space))
			verts.append(point(deg, RADIUS, disp + space * 2))

	# last one
	lastdisp = disp + space * 2
	disp = disp + space
	topcap.append(len(verts) - 1)
	for i in range(SUBDIVISIONS - 1):
		disp = disp + (STEP / SUBDIVISIONS) / 2
		deg = math.pi * 2 * i / SUBDIVISIONS
		verts.append(point(deg, RADIUS - IDENT, disp))
		topcap.append(len(verts))
		verts.append(point(deg, RADIUS, lastdisp))

	# SHOULDER (vertices and faces)
	if (SHOULDER > 0):
		for i in range(SUBDIVISIONS):
			deg = math.pi * 2 * (i - 1) / SUBDIVISIONS
			shouldercap.append(len(verts))
			verts.append(point(deg, RADIUS, lastdisp + SHOULDER))
		faces.append([shouldercap[0], shouldercap[SUBDIVISIONS - 1], topcap[SUBDIVISIONS - 1], topcap[0]])
		for i in range (SUBDIVISIONS - 1):
			faces.append([shouldercap[i], shouldercap[i+1], topcap[i+1], topcap[i]])
			
	else:
		# when we are doing the head, this will come handy
		shouldercap = topcap


	# ========= FACES =========
	# cap / 1st loop
	# 	face count: 16 (SUBDIVISIONS * 2)
	# 	vertice count: 22 (SUBDIVISIONS * 3 - 2)
	faces.append([1, 0, SUBDIVISIONS])	
	faces.append([0, SUBDIVISIONS + 1, SUBDIVISIONS])

	for i in range(1, SUBDIVISIONS):
		i2 = (i - 1) * 2
		if (i == SUBDIVISIONS - 1):
			faces.append([0, i, SUBDIVISIONS + i2, SUBDIVISIONS + i2+2])
		else:
			faces.append([i+1, i, SUBDIVISIONS + i2, SUBDIVISIONS + i2+2])
		faces.append([SUBDIVISIONS + i2 + 2, SUBDIVISIONS + i2, SUBDIVISIONS + i2 + 1, SUBDIVISIONS + i2 + 3])

	# 2nd to n - 1
	lastcorner = 0
	nextcorner = SUBDIVISIONS + 1
	othercorner = 0
	vcount = SUBDIVISIONS * 3 - 2
	for j in range(1, LOOPS - 1):
		for i in range(SUBDIVISIONS):
			othercorner = nextcorner + vcount - SUBDIVISIONS - 1
			faces.append([nextcorner, lastcorner, othercorner, othercorner+2])
			faces.append([othercorner+2, othercorner, othercorner+1, othercorner+3])
			lastcorner = nextcorner
			nextcorner = nextcorner + 2


	# last one!
	eol = othercorner + 3 # stands for end of line
	for i in range(1, SUBDIVISIONS):
		othercorner = nextcorner + vcount - SUBDIVISIONS - 1
		faces.append([nextcorner, lastcorner, othercorner, othercorner + 2])
		faces.append([othercorner+2, othercorner, othercorner+1, othercorner+3])

		if (i == SUBDIVISIONS - 1):
			faces.append([eol, nextcorner, othercorner+2])
			faces.append([eol, othercorner+2, othercorner+3])
		lastcorner = nextcorner
		nextcorner = nextcorner + 2

	return verts, faces

# Check if vertice is an outer edge.
def is_outer(OFFSET, EDGE, SUBDIVISIONS, VCOUNT):
	v1 = EDGE.v1.index - OFFSET
	v2 = EDGE.v2.index - OFFSET
	if ((v1 < 0) or (v2 < 0)):
		return False
	eol = VCOUNT - SUBDIVISIONS * 2 + 1
	v1_found = False
	v2_found = False
	
	if ((v1 == 0) or (v1 == eol)):
		v1_found = True
	
	if ((v2 == 0) or (v2 == eol)):
		v2_found = True
	
	idx = SUBDIVISIONS + 1
	while ((v1_found == False) or (v2_found == False)):
		if (idx > VCOUNT):
			return False
		if (v1 == idx):
			v1_found = True
		if (v2 == idx):
			v2_found = True
	
		idx = idx + 2
	
	return True

def is_inner(OFFSET, EDGE, SUBDIVISIONS, VCOUNT):
	v1 = EDGE.v1.index - OFFSET
	v2 = EDGE.v2.index - OFFSET
	if ((v1 < 0) or (v2 < 0)):
		return False
	eol = VCOUNT - SUBDIVISIONS * 2 + 1
	v1_found = False
	v2_found = False
	
	if ((v1 == 0) or (v1 == eol)):
		v1_found = True
	
	if ((v2 == 0) or (v2 == eol)):
		v2_found = True
	
	idx = SUBDIVISIONS
	while ((v1_found == False) or (v2_found == False)):
		if (idx > VCOUNT):
			return False
		if (v1 == idx):
			v1_found = True
		if (v2 == idx):
			v2_found = True
	
		idx = idx + 2
	
	return True

def is_cap(OFFSET, EDGE, SUBDIVISIONS, VCOUNT):
	v1 = EDGE.v1.index - OFFSET
	v2 = EDGE.v2.index - OFFSET

	if ((v1 < 0) or (v2 < 0)):
		return False

	eol = VCOUNT - SUBDIVISIONS * 2 + 1

	v1_found = False
	v2_found = False

	for i in range(SUBDIVISIONS):
		nx = eol + i*2
		if ((v1 == i) or (v1 == nx)):
			v1_found = True
		if ((v2 == i) or (v2 == nx)):
			v2_found = True

	if ((v1_found == False) or (v2_found == False)):
		return False

	return True



# main function (window handle and input variables)
def main():
	verts = []
	faces = []		

	boltRadiusInput = Blender.Draw.Create(1.0)
	boltDivInput = Blender.Draw.Create(8)
	boltStepInput = Blender.Draw.Create(.2)
	boltLoopsInput = Blender.Draw.Create(5)
	boltIdentInput = Blender.Draw.Create(.1)
	boltShoulderInput = Blender.Draw.Create(0.000)

	boltCreaseInsideInput = Blender.Draw.Create(0.000);
	boltCreaseOutsideInput = Blender.Draw.Create(0.000);
	boltCreaseCapInput = Blender.Draw.Create(0.000);
	
	block = []
	block.append(("Radius:", boltRadiusInput, 0.01, 100, "the radius"))
	block.append(("Divisions:", boltDivInput, 4, 100, "the bolt divisions"))
	block.append(("Step:", boltStepInput, .01, 100, "step length"))
	block.append(("Loops:", boltLoopsInput, 3, 100, "the height of the pipe"))
	block.append(("Ident:", boltIdentInput, .01, 100, "number of height divisions"))
	block.append(("Shoulder:", boltShoulderInput, .000, 100, "height of the shoulder"))
	block.append("Edge Creases");
	block.append(("Inside:", boltCreaseInsideInput, .00, 1.000, "inside crease"))
	block.append(("Outside:", boltCreaseOutsideInput, .00, 1.000, "outside crease"))
	block.append(("Cap:", boltCreaseCapInput, .00, 1.000, "cap crease"))
	
	if not Blender.Draw.PupBlock("Create thread", block):
		return

#	Generates the mesh
	verts, faces = create_bolt(boltRadiusInput.val,boltDivInput.val,boltStepInput.val,boltLoopsInput.val,boltIdentInput.val, boltShoulderInput.val)

	# Edge crease stuff
	# Most of this is "imported" from BPyAddMesh.py module from blender 2.44
	# We need to get the number of vertices of the existing object, if any.
	scn = bpy.data.scenes.active
	if scn.lib: return
	ob_act = scn.objects.active

	vert_offset = 0
	edge_offset = 0
	if EditMode():
		me = ob_act.getData(mesh=1)
		if me.multires:
			BPyMessages.Error_NoMeshMultiresEdit()
			return
		EditMode(0)
		
		me.sel = False
		
		vert_offset = len(me.verts)
		edge_offset = len(me.edges)
		# we dont need this
		# face_offset = len(me.faces)

	# ---


	# Adds the mesh to the scene
	BPyAddMesh.add_mesh_simple('Bolt', verts, [], faces)

	# Set the correct edge creases
	EditMode(0)
	SUBDIVISIONS = boltDivInput.val
	ob_act = scn.objects.active
	me = ob_act.getData(mesh=1)
	me.sel = False

	vcount = len(me.verts) - vert_offset
	INNER_CREASE = int(255 * boltCreaseInsideInput.val)
	OUTER_CREASE = int(255 * boltCreaseOutsideInput.val)
	CAP_CREASE = int(255 * boltCreaseCapInput.val)

	for ed in me.edges:
		# inner edge
		if (INNER_CREASE > 0):
			if (is_inner(vert_offset, ed, SUBDIVISIONS, vcount)):
				ed.crease = INNER_CREASE

		# outer edge
		if (OUTER_CREASE > 0):
			if (is_outer(vert_offset, ed, SUBDIVISIONS, vcount)):
				ed.crease = OUTER_CREASE

		# cap edge
		if (CAP_CREASE > 0):
			if (is_cap(vert_offset, ed, SUBDIVISIONS, vcount)):
				ed.crease = CAP_CREASE


	EditMode(1)
		
	
	


# call our main function	
main()

